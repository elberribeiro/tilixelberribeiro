package com.example.elber.tilixelber

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ActivityRequisicao
import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.google.gson.GsonBuilder


class Principal : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    var retornos: ArrayList<ResponseListaInfos> = ArrayList()
    val FILENAME = "storage.json"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_requisicao)


        var retorno: InfosLista = ActivityRequisicao().execute().get();
        val fos =  applicationContext.openFileOutput(FILENAME, Context.MODE_PRIVATE)
        if (retorno != null) {
            var jj:json = json()
            val gsonBuilder = GsonBuilder()
            val gson = gsonBuilder.create()
            var json2 = gson.toJson(jj.dados)


        }
        fos.close()

        viewManager = LinearLayoutManager(this)
        viewAdapter = Adapter_Artigos(retorno.informacoes)
        recyclerView = findViewById<RecyclerView>(R.id.rc_artigos).apply {

            setHasFixedSize(true)

            layoutManager = viewManager
            adapter = viewAdapter
            retornos = retorno.informacoes


        }
    }

}